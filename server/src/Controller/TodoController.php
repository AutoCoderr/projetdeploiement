<?

namespace App\Controller;

use App\Repository\TodoRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Todo;
use Symfony\Component\Routing\Annotation\Route;

class TodoController extends AbstractController
{

    /**
     * @Route("/todo", name="list_todo", methods={"GET"})
     */
    public function listTodo(TodoRepository $todoRepository): Response
    {
        $todos = [];
        foreach ($todoRepository->findAll() as $todo) {
            $todos[] = ["id" => $todo->getId(), "name" => $todo->getName()];
        }

        return new JsonResponse($todos, 200);
    }

    /**
     * @Route("/todo", name="add_todo", methods={"POST"})
     */
    public function addTodo(Request $request): Response
    {
        $name = $request->get("name");
        if ($name == NULL)
            return new Response("Bad request", 400);

        $todo = new Todo();
        $todo->setName($name);

        $em = $this->getDoctrine()->getManager();
        $em->persist($todo);
        $em->flush();

        return new JsonResponse(['id '=> $todo->getId()], 200);
    }

    /**
     * @Route("/todo/{id}", name="update_todo", methods={"PUT"})
     */
    public function updateTodo(Request $request, TodoRepository $todoRepository, int $id): Response
    {
        $todo = $todoRepository->find($id);
        $name = $request->get("name");
        if ($name == NULL || $todo == NULL)
            return new Response("Bad request", 400);

        $todo->setName($name);

        $this->getDoctrine()->getManager()->flush();

        return new Response("Updated!", 200);
    }

    /**
     * @Route("/todo/{id}", name="remove_todo", methods={"DELETE"})
     */
    public function removeTodo(TodoRepository $todoRepository, int $id): Response
    {
        $todo = $todoRepository->find($id);
        if ($todo == NULL) {
            return new Response("Not found", 404);
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($todo);

        $em->flush();;

        return new Response("Deleted!", 204);
    }
}
