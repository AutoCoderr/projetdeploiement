## Rebuild à chaque pull, pour avoir les images à jour

- docker-compose build

##Lancer le docker

- docker-compose up -d

##Accéder au site

back : http://127.0.0.1:3001
front : http://127.0.0.1:3000

preprod_back : https://cicd-5iw1-preprod-back.herokuapp.com/
preprod_front : https://cicd-5iw1-preprod-front.herokuapp.com/