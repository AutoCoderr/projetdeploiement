/*global Vue, todoStorage */

(function (exports) {

	'use strict';

	var filters = {
		all: function (todos) {
			return todos;
		},
		active: function (todos) {
			return todos.filter(function (todo) {
				return !todo.completed;
			});
		},
		completed: function (todos) {
			return todos.filter(function (todo) {
				return todo.completed;
			});
		}
	};

	exports.app = new Vue({

		// the root element that will be compiled
		el: '.todoapp',

		// app initial state
		data: {
			todos: [],
			newTodo: '',
			editedTodo: null,
			visibility: 'all',
			backendUrl: null
		},

		// watch todos change tfor localStorage persistence
		watch: {
			todos: {
				deep: true,
				handler: todoStorage.save
			}
		},

		// computed properties
		// http://vuejs.org/guide/computed.html
		computed: {
			filteredTodos: function () {
				return filters[this.visibility](this.todos);
			},
			remaining: function () {
				return filters.active(this.todos).length;
			},
			allDone: {
				get: function () {
					return this.remaining === 0;
				},
				set: function (value) {
					this.todos.forEach(function (todo) {
						todo.completed = value;j
					});
				}
			}
		},

		beforeMount() {
			this.getConfigJson().then(() => this.getTodos())
		},

		// methods that implement data logic.
		// note there's no DOM manipulation here at all.
		methods: {

			pluralize: function (word, count) {
				return word + (count === 1 ? '' : 's');
			},
			
			getTodos: function () {
				fetch(this.backendUrl)
				.then(res => res.json())
				.then(todos => this.todos = todos)
			},

			getConfigJson: function () {
				return fetch("/config.json")
					.then(res => res.json())
					.then(config => {
						for (let key of config) {
							this[key] = config[key]
						}
					})
			},

			addTodo: function () {
				var value = this.newTodo && this.newTodo.trim();
				if (!value) {
					return;
				}
				fetch(this.backendUrl, {
					method:"POST",
					headers: {
						'Content-Type':  'application/x-www-form-urlencoded;charset=UTF-8'
					},
					body: new URLSearchParams({
						name: value
					})
				}).then(res => res.json())
					.then(({id}) => {
						this.todos.push({ id, name: value, completed: false });
						this.newTodo = '';
					})
				
			},

			removeTodo: function (todoToDelete) {
				fetch(this.backendUrl+"/"+todoToDelete.id, {
					method: "DELETE"
				}).then(_ => this.todos = this.todos.filter(todo => todo.id !== todoToDelete.id))
			},

			editTodo: function (todo) {
				this.beforeEditCache = todo.name;
				this.editedTodo = todo;
			},

			doneEdit: function (todo) {
				if (!this.editedTodo) {
					return;
				}
				fetch(this.backendUrl+"/"+todo.id, {
					method: "PUT",
					headers: {
						'Content-Type':  'application/x-www-form-urlencoded;charset=UTF-8'
					},
					body: new URLSearchParams({
						name: todo.name
					})
				}).then(_ => {
					this.editedTodo = null;
					todo.name = todo.name.trim();
					if (!todo.name) {
						this.removeTodo(todo);
					}
				})
			},

			cancelEdit: function (todo) {
				this.editedTodo = null;
				todo.name = this.beforeEditCache;
			},

			removeCompleted: function () {
				this.todos = filters.active(this.todos);
			}
		},

		// a custom directive to wait for the DOM to be updated
		// before focusing on the input field.
		// http://vuejs.org/guide/custom-directive.html
		directives: {
			'todo-focus': function (el, binding) {
				if (binding.value) {
					el.focus();
				}
			}
		},
                created: function () {
                    todoStorage.fetch().then(todos => { this.todos = todos; });
                }
	});

})(window);
